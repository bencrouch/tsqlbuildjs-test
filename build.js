let debug = false

const fs = require('fs')
const sql = require('mssql')

let connection = null
let sprocs = 0
let patches = 0
let errors = []

async function main() {
    try {
        let args = process.argv.slice(2);
        let config = require('./config')

        if(!config) {
            throw { message: 'No config found or config in invalid form. Create config.js in root directory or copy config-example.js. See readme for more.'}
        }

        if(config.debug)
            debug = config.debug

        if(args.length > 0) {
            for(arg of args) {
                let conf = config.builds.find(function(el) {
                    return el.name.toLowerCase() == arg.toLowerCase();
                });

                if(conf) {
                    console.log(`Building ${conf.name}:`)
                    await run(conf)
                } else {
                    console.log(`Building ${arg}:`)
                    throw { message: 'No project in config for that argument.'}
                }
            }
        } else {
            console.log(`Building ${config.builds[0].name}:`)
            await run(config.builds[0])
        }

    } catch (e) {
        console.log('\x1b[31m%s\x1b[0m', 'An error occured: ' + e.message)
        if(debug)
            console.log(e)
        process.exit(1);
    }

    process.exit()
}

async function run(config) {
    try {
        console.log(`======================================================================`)
        console.log(`(Step 0/2) --> Attempt to establish connection with database for ${config.name}...`)


        let connection = await sql.connect(`mssql://${config.userid}:${config.password}@${config.server}/${config.database}`)
        await sql.close()

        let version = await getVersion(config)

        
        console.log(`Connection established to '${config.server}/${config.database}'. Current patch version '${version}'.`)
        console.log(`----------------------------------------------------------------------`)

        console.log('(Step 1/2) --> Start executing patches... ')
        await runPatches(config, version)

        console.log('(Step 2/2) --> Start executing sprocs... ')
        //await runSprocs(config)

        console.log('\x1b[32m%s\x1b[0m', 'Database has been successfully updated!')
        console.log(`======================================================================`)
    } catch (e) {
        throw e;
    }
}

//
//
//

async function runSprocs(conf) {
    try {
        let files = getFiles(conf.path + 'Sprocs', '')
        for(file of files) {
            let data = getFileData(conf.path + 'Sprocs', file)
            if(data.length > 0) {
                let rows = await execute(file, data, conf)
                sprocs++
            } else {
                errors.push(`Failed execute: ${file} - Sproc empty`)
            }
        }

        console.log(`----------------------------------------------------------------------`)
        console.log('\x1b[32m%s\x1b[0m', `Sprocs executed: ( ${sprocs} )`)
         
        if(errors.length > 0) {
            console.log('\x1b[31m%s\x1b[0m', `Sprocs failed: ( ${errors.length} )`)
            for(e of errors) {
                console.log(e)
            }
        }
        console.log(`----------------------------------------------------------------------`)

        return
    } catch (e) {
        throw e
    }
}

async function runPatches(conf, v) {
    try {
        let versions = getPatches(conf, v)
        //console.log(versions)
        if(versions.length > 0) {
            for(version of versions) {
                console.log('\x1b[34m%s\x1b[0m', `Executing patches in version '${version}'...`)
                let files = getFiles(conf.path + 'Patches', version)
                for(file of files) {
                    let data = getFileData(conf.path + 'Patches', file, version);
                    let rows = await execute(file, data, conf)
                    patches++
                }
                await updateVersion(conf, version)
            }
            console.log(`Patches executed successfully! (${patches} total)`)
        } else {
            console.log(`Database is already on most recent version (${v}).`)
        }

        console.log(`----------------------------------------------------------------------`)

        return
    } catch (e) {
        throw e
    }
}

function getPatches(conf, version) {
    return fs.readdirSync(`.${conf.path}/Patches`).filter(function (file) {
        return fs.statSync(`.${conf.path}/Patches/${file}`).isDirectory();
    }).sort().filter(patch => patch > version);
}

function getFiles(type, version) {
    return fs.readdirSync(`./${type}/${version}`).filter(function (file) {
        return fs.statSync(`./${type}/${version}/${file}`).isFile();
    }).sort();
}

function getFileData(type, file, version) {
    let d = ''
    if(type === 'Patches')
        d = fs.readFileSync(`./${type}/${version}/${file}`)
    else
        d = fs.readFileSync(`./${type}/${file}`)
    return d.toString()
}

//
// Executes Stored Procedure or Patch contents.
//
async function execute(file, fullData, config) {
    try {
        let pool = await sql.connect(`mssql://${config.userid}:${config.password}@${config.server}/${config.database}`)
        let pre_query = new Date().getTime()
        let data = fullData.split('\r\nGO\r')
        //console.log(data)
        for(d of data) {
            let res = await pool.request().query(d)
        }
        let post_query = new Date().getTime()
        console.log(`Executed: ${file} (${(post_query - pre_query)}ms)`)
        await sql.close()
        return
    } catch (e) {
        console.log('\x1b[31m%s\x1b[0m', 'An error occured executing ' + file + '')
        throw e
    }
}


//
// Returns current database patch number. If null, returns 0.
//
async function getVersion(conf) {
    try {
        let pool = await sql.connect(`mssql://${conf.userid}:${conf.password}@${conf.server}/${conf.database}`)
        let res = await pool.request().query(`select [value] from sys.extended_properties where [name] = 'Version'`)
        await sql.close()

        if(res && res.recordset[0]) {
            return res.recordset[0].value
        } else {
            return '0'
        }
    } catch (e) {
        throw e
    }
}

//
// Creates or Updates database patch number using the 'Version' extended property.
//
async function updateVersion(conf, version) {
    try {
        let pool = await sql.connect(`mssql://${conf.userid}:${conf.password}@${conf.server}/${conf.database}`)
        
        let res = await pool.request().query(`select [value] from sys.extended_properties where [name] = 'Version'`)
        if(res.recordset.length <= 0) {
            res = await pool.request().query(`exec sp_addextendedproperty @name = 'Version', @value = '${version}'`)
        } else {
            res = await pool.request().query(`exec sp_updateextendedproperty @name = 'Version', @value = '${version}'`)
        }
        await sql.close()

        console.log(`Database has been updated to version ${version}.`)

        return
    } catch (e) {
        throw e
    }
}

main()