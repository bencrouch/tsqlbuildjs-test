Patch versions should increment within folders such as
'00.00.00' -> '00.00.01' 

Patches within
a version should follow names such as:

'00.what_changed.patch.sql' 
'01.what_else_changed.patch.sql'
or 
'02.changed_table.table.sql